<?php

namespace App\Form;

use App\Entity\ShopUserSettings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ShopUserSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logoFile', VichImageType::class)
            ->add('backgroundFile', VichImageType::class)
            ->add('bannerFile', VichImageType::class)
            ->add('userPictureFile', VichImageType::class)
            ->add('resultsPictureFile', VichImageType::class)
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ShopUserSettings::class,
        ]);
    }
}
