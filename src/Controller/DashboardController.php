<?php

namespace App\Controller;

use App\Entity\ShopOrders;
use App\Entity\ShopProducts;
use App\Entity\ShopUsers;
use App\Entity\ShopUserSettings;
use App\Entity\User;
use App\Form\ShopProductsType;
use App\Form\ShopUserSettingsType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index()
    {
        return $this->render('dashboard/index/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }

    /**
     * @Route("/dashboard/primo-avvio/", name="first-run")
     */
    public function firstRun()
    {
        return $this->render('dashboard/wizard/firstrun.html.twig');
    }

    /**
     * @Route("/dashboard/ordini/lista", name="dashboard_orders_list")
     */
    public function ordersList()
    {
        return $this->render('dashboard/orders/list.html.twig');
    }

    /**
     * @Route("/dashboard/ordini/mostra/{id}", name="dashboard_orders_show")
     */
    public function ordersShow($id)
    {
        $order = $this->getDoctrine()->getRepository(ShopOrders::class)->findOneBy(['id' => $id]);

        return $this->render('dashboard/orders/show.html.twig',[
            'order' => $order
        ]);
    }

    /**
     * @Route("/dashboard/prodotti/lista", name="dashboard_products_list")
     */
    public function productsList()
    {
        return $this->render('dashboard/products/list.html.twig');
    }

    /**
     * @Route("/dashboard/prodotti/nuovo", name="dashboard_products_new")
     */
    public function productsNew(Request $request, EntityManagerInterface $em)
    {
        $product = new ShopProducts();


        $form = $this->createForm(ShopProductsType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $product->setCreatedAt(new \DateTime("now"));
            $product->setUpdatedAt(new \DateTime("now"));
            $product->setSeller($this->getUser()->getShopUsers());

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('dashboard_products_list');
        }

        return $this->render('dashboard/products/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/dashboard/prodotti/modifica/{id}", name="dashboard_products_edit")
     */
    public function productsEdit(Request $request, EntityManagerInterface $em, $id)
    {
        $product = $this->getDoctrine()->getRepository(ShopProducts::class)->findOneBy(['id' => $id]);


        $form = $this->createForm(ShopProductsType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $product->setCreatedAt(new \DateTime("now"));
            $product->setUpdatedAt(new \DateTime("now"));
            $product->setSeller($this->getUser()->getShopUsers());

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('dashboard_products_list');
        }

        return $this->render('dashboard/products/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/dashboard/impostazioni", name="dashboard_settings")
     */
    public function settings(Request $request, EntityManagerInterface $em)
    {
        $settings = $this->getDoctrine()->getRepository(ShopUserSettings::class)->findOneBy(['user' => $this->getUser()]);
        $user = $this->getDoctrine()->getRepository(ShopUsers::class)->findOneBy(['user' => $this->getUser()]);

        $form = $this->createForm(ShopUserSettingsType::class, $settings);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if ($settings != null)
            {
                $settings->setUpdatedAt(new \DateTime("now"));
                $em->flush();
            } else {
                dump($user);
                $settings = new ShopUserSettings();
                $settings->setUser($user);
                $settings->setUpdatedAt(new \DateTime("now"));
                $em->persist($settings);
                $em->flush();
            }
            return $this->redirectToRoute('dashboard_settings');
        }

        return $this->render('dashboard/settings/settings.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/dashboard/utenti", name="dashboard_users")
     */
    public function users()
    {
        return $this->render('dashboard/users/list.html.twig', []);
    }

    /**
     * @Route("/dashboard/negozi", name="dashboard_shops")
     */
    public function shops()
    {
        return $this->render('dashboard/users/shops.html.twig', []);
    }
}
