<?php

namespace App\Controller;

use App\Entity\ShopCategories;
use App\Entity\ShopOrders;
use App\Entity\ShopUsers;
use App\Entity\User;
use App\Repository\ShopProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/api/v3/")
 */
class APIController extends AbstractController
{
    private $productsRepo;

    public function __construct(ShopProductsRepository $productsRepo)
    {
        $this->productsRepo = $productsRepo;
    }

    /**
     * @Route("products/list/byUser/{user}",name="APIproductsList", methods={"GET"})
     */
    public function productsList(Request $request,$user): JsonResponse
    {
        $products = $this->productsRepo->findBy(['seller' => $user]);

        $categories = [];
        foreach ($products->getCategory() as $category)
        {
            array_push($categories,$category);
        }

        $data =  [
            'id' => $products->getId(),
            'name' => $products->getName(),
            'description' => $products->getDescription(),
            'category' => $categories,
            'price' => $products->getPrice(),
            'vat' => $products->getVat(),
            'seller' => $products->getSeller(),
            'pics' => $products->getPics(),
            'shipmentServices' => $products->getShipmentServices(),
            'isActive' => $products->getIsActive(),
            'inStock' => $products->getInStock(),
            'createdAt' => $products->getCreatedAt(),
            'updatedAt' => $products->getUpdatedAt()
        ];
        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("products/list/all", name="APIproductsListALL", methods={"GET","POST"})
     */
    public function getAll(Request $request): JsonResponse
    {
        $products = $this->productsRepo->findAll();
        $data = [];
        $intl = new \IntlDateFormatter($request->getLocale(), \IntlDateFormatter::LONG, \IntlDateFormatter::NONE, null, null, 'd-LLL-y');

        foreach ($products as $product) {
            //$cat = $this->getDoctrine()->getRepository(ShopCategories::class)->findBy(['id' => $product->getCategory()]);
            //dump($product);
            //var_dump($product->getCategory());
            $data[] =  [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'category' => $product->getCategory()[0]->getName(),
                'price' => $product->getPrice(),
                'vat' => $product->getVat(),
                'seller' => $product->getSeller(),
                'image' => $product->getImage(),
                'shipmentServices' => $product->getShipmentServices(),
                'isActive' => $product->getIsActive(),
                'inStock' => $product->getInStock(),
                'createdAt' => $product->getCreatedAt(),
                'updatedAt' => $intl->format($product->getUpdatedAt(), 'd-M-Y H:m')
            ];

        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("orders/list/all", name="APIordersListALL")
     */
    public function ordersList() {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $orders = $this->getDoctrine()->getRepository(ShopOrders::class)->findAll();
        } else {
            $orders = $this->getDoctrine()->getRepository(ShopOrders::class)->findBy(['seller' => $this->getSeller()]);
        }
        $data = [];

        foreach ($orders as $order)
        {
            $data[] = [
                'id' => $order->getId(),
                'user' => $order->getUser()->getUserName(),
                'seller' => $order->getSeller()->getShopName(),
                'date' => date_format($order->getDate(), 'd-M-Y H:m'),
                'shipmentMethod' => $order->getShipment(),
                'total' => $order->getTotal(),
                'status' => $order->getStatus(),
                'payment' => $order->getPayment(),
                'buyer_message' => $order->getBuyerMessage(),
                'items' => $order->getItems(),
            ];
        }
        return new JsonResponse($data, Response::HTTP_OK);

    }

    /**
     * @Route("getSellersList/{lat}/{lon}", name="APIUtilsGetSellersList")
     */
    public function getSellersList($lat,$lon, EntityManagerInterface $em)
    {
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from(ShopUsers::class,'s')
            ->where('100 >= 6530 * ACOS(SIN(s.latitude*:sf)*SIN(:lat*:sf) + COS(s.latitude*:sf)*COS(:lat*:sf)*COS((s.longitude-:lon)*:sf))')
            ->orderBy('ACOS(SIN(s.latitude*:sf)*SIN(:lat*:sf) + COS(s.latitude*:sf)*COS(:lat*:sf)*COS((s.longitude-:lon)*:sf))')
            ->setParameter(':lat', $lat)
            ->setParameter(':lon', $lon)
            ->setParameter('sf', '(3.14159 / 180)');
        $query = $qb->getQuery();

        $result = $query->getResult();

        return $this->render('api/shopList.html.twig', [
            'results' => $result,
            'lat' => $lat,
            'lon' => $lon,
            'fun' => $this
        ]);
    }

    /**
     * @Route("users/list/all", name="APIusersListALL")
     */
    public function getUsersList()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        } else {

        }

        $data = [];

        foreach ($users as $user)
        {
            $data[] = [
                'id' => $user->getId(),
                'name' => $user->getEmail(),
                'datejoin' => date_format($user->getDatejoin(), 'd-M-Y H:m'),
                'isActive' => $user->getIsActive(),
                'city' => $user->getCity(),
                'phone' => $user->getPhone(),
                'role' => $user->getRoles(),
            ];
        }
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("users/shops/all", name="APIshopsListALL")
     */
    public function getShopsList()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $shops = $this->getDoctrine()->getRepository(ShopUsers::class)->findAll();
        } else {

        }

        $data = [];

        foreach ($shops as $shop)
        {
            $data[] = [
                'id' => $shop->getId(),
                'name' => $shop->getShopName(),
                'isActive' => $shop->getIsActive(),
                'address' => $shop->getAddress(),
                'phone' => $shop->getPhone(),
                'email' => $shop->getEmail(),
            ];
        }
        return new JsonResponse($data, Response::HTTP_OK);
    }

    private function getSeller()
    {
        return $this->getDoctrine()->getRepository(ShopUsers::class)->findOneBy(['user' => $this->getUser()->getID()]);
    }

    function getDistanceBetweenPoints($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {

        $rad = M_PI / 180;
        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin($latitudeFrom * $rad)
            * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
            * cos($latitudeTo * $rad) * cos($theta * $rad);

        return acos($dist) / $rad * 60 *  1.853;

    }

    function getDistance($lat1,$lon1,$lat2,$lon2) {
        $API_KEY = "AIzaSyCwK7uvRQHIpHQIq_lL7njWi6al9W_ebVE";
        $response = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $lat1 . ',' . $lon1 . '&destinations=' . $lat2 . ',' . $lon2 .'&key=' . $API_KEY);
        $response = json_decode($response,true);
        dump($response);
        return $response["rows"][0]["elements"][0]["distance"]["text"];
    }
}
