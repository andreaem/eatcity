<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardFragmentsController extends AbstractController
{
    /**
     * @Route("/dashboard/fragments", name="dashboard_fragments")
     */
    public function aside()
    {
        $menu = [
            0 => [
                'name' => 'Dashboard',
                'icon' => 'flaticon2-protection',
                'url' => 'dashboard'
            ],
            1 => [
                'name' => 'Ordini',
                'icon' => 'flaticon-truck',
                'url' => 'dashboard_orders_list'
            ],
            2 => [
                'name' => 'Prodotti',
                'icon' => 'flaticon2-box-1',
                'url' => 'dashboard',
                'sub' => [
                    0 => [
                        'name' => 'Lista',
                        'url' => 'dashboard_products_list',
                        'role' => 'ROLE_SELLER'
                    ],
                    1 => [
                        'name' => 'Categorie',
                        'url' => 'dashboard',
                        'role' => 'ROLE_ADMIN'
                    ]
                ]
            ],
        ];

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            array_push($menu, [
                    'name' => 'Utenti',
                    'icon' => 'flaticon2-user',
                    'url' => 'dashboard_users',
                    'sub' => [
                        0 => [
                            'name' => 'Lista Utenti',
                            'url' => 'dashboard_users',
                            'role' => 'ROLE_ADMIN'
                        ],
                        1 => [
                            'name' => 'Lista Negozi',
                            'url' => 'dashboard_shops',
                            'role' => 'ROLE_ADMIN'
                        ]
                    ]
            ]);
        }

        array_push($menu, [
            'name' => 'Impostazioni',
            'icon' => 'flaticon-cogwheel-2',
            'url' => 'dashboard_settings'
        ]);

        return $this->render('dashboard/template/aside.html.twig', [
            'menu' => $menu
        ]);
    }
}
