<?php

namespace App\Repository;

use App\Entity\ShopCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ShopCategoriesRepository extends \Doctrine\ORM\EntityRepository
{
    public function nameToId($name) {
        $item = $this->getEntityManager()->getRepository(ShopCategories::class)->findBy(['name' => $name]);
        return $item[0]->getId();
    }
}
