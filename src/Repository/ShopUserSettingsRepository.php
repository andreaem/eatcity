<?php

namespace App\Repository;

use App\Entity\ShopUserSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShopUserSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopUserSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopUserSettings[]    findAll()
 * @method ShopUserSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopUserSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopUserSettings::class);
    }

    // /**
    //  * @return ShopUserSettings[] Returns an array of ShopUserSettings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShopUserSettings
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
