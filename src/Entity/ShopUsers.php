<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopUsersRepository")
 */
class ShopUsers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shopUsers", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShopUsersInvoices", mappedBy="user")
     */
    private $shopUsersInvoices;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShopProducts", mappedBy="user")
     */
    private $shopProducts;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shop_name;

    /**
     * @ORM\OneToOne(targetEntity=ShopUserSettings::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $shopUserSettings;

    /**
     * @ORM\OneToMany(targetEntity=ShopOrders::class, mappedBy="seller")
     */
    private $shopOrders;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity=ShopSubCategories::class, mappedBy="shop")
     */
    private $category;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $openingTime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $closingDay;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $closingTime;

    public function __construct()
    {
        $this->shopUsersInvoices = new ArrayCollection();
        $this->shopProducts = new ArrayCollection();
        $this->shopOrders = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUser()->getRealName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ShopUsersInvoices[]
     */
    public function getShopUsersInvoices(): Collection
    {
        return $this->shopUsersInvoices;
    }

    public function addShopUsersInvoice(ShopUsersInvoices $shopUsersInvoice): self
    {
        if (!$this->shopUsersInvoices->contains($shopUsersInvoice)) {
            $this->shopUsersInvoices[] = $shopUsersInvoice;
            $shopUsersInvoice->setUser($this);
        }

        return $this;
    }

    public function removeShopUsersInvoice(ShopUsersInvoices $shopUsersInvoice): self
    {
        if ($this->shopUsersInvoices->contains($shopUsersInvoice)) {
            $this->shopUsersInvoices->removeElement($shopUsersInvoice);
            // set the owning side to null (unless already changed)
            if ($shopUsersInvoice->getUser() === $this) {
                $shopUsersInvoice->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ShopProducts[]
     */
    public function getShopProducts(): Collection
    {
        return $this->shopProducts;
    }

    public function addShopProduct(ShopProducts $shopProduct): self
    {
        if (!$this->shopProducts->contains($shopProduct)) {
            $this->shopProducts[] = $shopProduct;
            $shopProduct->setUser($this);
        }

        return $this;
    }

    public function removeShopProduct(ShopProducts $shopProduct): self
    {
        if ($this->shopProducts->contains($shopProduct)) {
            $this->shopProducts->removeElement($shopProduct);
            // set the owning side to null (unless already changed)
            if ($shopProduct->getUser() === $this) {
                $shopProduct->setUser(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getShopName(): ?string
    {
        return $this->shop_name;
    }

    public function setShopName(?string $shop_name): self
    {
        $this->shop_name = $shop_name;

        return $this;
    }

    public function getShopUserSettings(): ?ShopUserSettings
    {
        return $this->shopUserSettings;
    }

    public function setShopUserSettings(?ShopUserSettings $shopUserSettings): self
    {
        $this->shopUserSettings = $shopUserSettings;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $shopUserSettings ? null : $this;
        if ($shopUserSettings->getUser() !== $newUser) {
            $shopUserSettings->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return Collection|ShopOrders[]
     */
    public function getShopOrders(): Collection
    {
        return $this->shopOrders;
    }

    public function addShopOrder(ShopOrders $shopOrder): self
    {
        if (!$this->shopOrders->contains($shopOrder)) {
            $this->shopOrders[] = $shopOrder;
            $shopOrder->setSeller($this);
        }

        return $this;
    }

    public function removeShopOrder(ShopOrders $shopOrder): self
    {
        if ($this->shopOrders->contains($shopOrder)) {
            $this->shopOrders->removeElement($shopOrder);
            // set the owning side to null (unless already changed)
            if ($shopOrder->getSeller() === $this) {
                $shopOrder->setSeller(null);
            }
        }

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|ShopSubCategories[]
     */
    public function getName(): Collection
    {
        return $this->name;
    }

    public function addCategory(ShopSubCategories $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->setShop($this);
        }

        return $this;
    }

    public function removeCategroy(ShopSubCategories $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
            // set the owning side to null (unless already changed)
            if ($category->getShop() === $this) {
                $category->setShop(null);
            }
        }

        return $this;
    }

    public function getOpeningTime(): ?\DateTimeInterface
    {
        return $this->openingTime;
    }

    public function setOpeningTime(?\DateTimeInterface $openingTime): self
    {
        $this->openingTime = $openingTime;

        return $this;
    }

    public function getClosingDay(): ?int
    {
        return $this->closingDay;
    }

    public function setClosingDay(?int $closingDay): self
    {
        $this->closingDay = $closingDay;

        return $this;
    }

    public function getClosingTime(): ?\DateTimeInterface
    {
        return $this->closingTime;
    }

    public function setClosingTime(?\DateTimeInterface $closingTime): self
    {
        $this->closingTime = $closingTime;

        return $this;
    }
}
