<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopProductsRepository")
 * @Vich\Uploadable
 */
class ShopProducts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShopCategories", inversedBy="shopProducts")
     */
    private $category;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $inStock;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShopShipments")
     */
    private $shipmentServices;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $business_user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopUsers", inversedBy="shopProducts")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShopCart", mappedBy="item")
     */
    private $shopCarts;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShopOrders", mappedBy="items")
     */
    private $shopOrders;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopUsers", inversedBy="shopProducts")
     */
    private $seller;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->shipmentServices = new ArrayCollection();
        $this->business_user = new ArrayCollection();
        $this->shopCarts = new ArrayCollection();
        $this->shopOrders = new ArrayCollection();
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'category' => $this->getCategory(),
            'price' => $this->getPrice(),
            'vat' => $this->getVat(),
            'seller' => $this->getSeller(),
            'shipmentServices' => $this->getShipmentServices(),
            'isActive' => $this->getIsActive(),
            'inStock' => $this->getInStock(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        ];
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ShopCategories[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(ShopCategories $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(ShopCategories $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getInStock(): ?int
    {
        return $this->inStock;
    }

    public function setInStock(?int $inStock): self
    {
        $this->inStock = $inStock;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|ShopShipments[]
     */
    public function getShipmentServices(): Collection
    {
        return $this->shipmentServices;
    }

    public function addShipmentService(ShopShipments $shipmentService): self
    {
        if (!$this->shipmentServices->contains($shipmentService)) {
            $this->shipmentServices[] = $shipmentService;
        }

        return $this;
    }

    public function removeShipmentService(ShopShipments $shipmentService): self
    {
        if ($this->shipmentServices->contains($shipmentService)) {
            $this->shipmentServices->removeElement($shipmentService);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getBusinessUser(): Collection
    {
        return $this->business_user;
    }

    public function addBusinessUser(User $businessUser): self
    {
        if (!$this->business_user->contains($businessUser)) {
            $this->business_user[] = $businessUser;
        }

        return $this;
    }

    public function removeBusinessUser(User $businessUser): self
    {
        if ($this->business_user->contains($businessUser)) {
            $this->business_user->removeElement($businessUser);
        }

        return $this;
    }

    public function getUser(): ?ShopUsers
    {
        return $this->user;
    }

    public function setUser(?ShopUsers $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|ShopCart[]
     */
    public function getShopCarts(): Collection
    {
        return $this->shopCarts;
    }

    public function addShopCart(ShopCart $shopCart): self
    {
        if (!$this->shopCarts->contains($shopCart)) {
            $this->shopCarts[] = $shopCart;
            $shopCart->addItem($this);
        }

        return $this;
    }

    public function removeShopCart(ShopCart $shopCart): self
    {
        if ($this->shopCarts->contains($shopCart)) {
            $this->shopCarts->removeElement($shopCart);
            $shopCart->removeItem($this);
        }

        return $this;
    }

    public function getVat(): ?int
    {
        return $this->vat;
    }

    public function setVat(?int $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return Collection|ShopOrders[]
     */
    public function getShopOrders(): Collection
    {
        return $this->shopOrders;
    }

    public function addShopOrder(ShopOrders $shopOrder): self
    {
        if (!$this->shopOrders->contains($shopOrder)) {
            $this->shopOrders[] = $shopOrder;
            $shopOrder->addItem($this);
        }

        return $this;
    }

    public function removeShopOrder(ShopOrders $shopOrder): self
    {
        if ($this->shopOrders->contains($shopOrder)) {
            $this->shopOrders->removeElement($shopOrder);
            $shopOrder->removeItem($this);
        }

        return $this;
    }

    public function getSeller(): ?ShopUsers
    {
        return $this->seller;
    }

    public function setSeller(?ShopUsers $seller): self
    {
        $this->seller = $seller;

        return $this;
    }
}
