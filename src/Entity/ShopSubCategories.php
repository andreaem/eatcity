<?php

namespace App\Entity;

use App\Repository\ShopSubCategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ShopSubCategoriesRepository::class)
 */
class ShopSubCategories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ShopUsers::class, inversedBy="name")
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=ShopSubCategories::class, inversedBy="father")
     */
    private $shopSubCategories;

    /**
     * @ORM\OneToMany(targetEntity=ShopSubCategories::class, mappedBy="shopSubCategories")
     */
    private $father;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    public function __construct()
    {
        $this->father = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShop(): ?ShopUsers
    {
        return $this->shop;
    }

    public function setShop(?ShopUsers $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShopSubCategories(): ?self
    {
        return $this->shopSubCategories;
    }

    public function setShopSubCategories(?self $shopSubCategories): self
    {
        $this->shopSubCategories = $shopSubCategories;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFather(): Collection
    {
        return $this->father;
    }

    public function addFather(self $father): self
    {
        if (!$this->father->contains($father)) {
            $this->father[] = $father;
            $father->setShopSubCategories($this);
        }

        return $this;
    }

    public function removeFather(self $father): self
    {
        if ($this->father->contains($father)) {
            $this->father->removeElement($father);
            // set the owning side to null (unless already changed)
            if ($father->getShopSubCategories() === $this) {
                $father->setShopSubCategories(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
