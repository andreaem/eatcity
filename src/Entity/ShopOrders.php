<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopOrdersRepository")
 */
class ShopOrders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="shopOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShopProducts", inversedBy="shopOrders")
     */
    private $items;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopShipments", inversedBy="shopOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shipment;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $payment;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $buyer_message;

    /**
     * @ORM\ManyToOne(targetEntity=ShopUsers::class, inversedBy="shopOrders")
     */
    private $seller;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $itemsQty = [];

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|ShopProducts[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ShopProducts $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
        }

        return $this;
    }

    public function removeItem(ShopProducts $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getShipment(): ?ShopShipments
    {
        return $this->shipment;
    }

    public function setShipment(?ShopShipments $shipment): self
    {
        $this->shipment = $shipment;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPayment(): ?int
    {
        return $this->payment;
    }

    public function setPayment(int $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getBuyerMessage(): ?string
    {
        return $this->buyer_message;
    }

    public function setBuyerMessage(?string $buyer_message): self
    {
        $this->buyer_message = $buyer_message;

        return $this;
    }

    public function getSeller(): ?ShopUsers
    {
        return $this->seller;
    }

    public function setSeller(?ShopUsers $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getItemsQty(): ?array
    {
        return $this->itemsQty;
    }

    public function setItemsQty(?array $itemsQty): self
    {
        $this->itemsQty = $itemsQty;

        return $this;
    }
}
