<?php

namespace App\Entity;

use App\Repository\ShopUserSettingsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass=ShopUserSettingsRepository::class)
 * @Vich\Uploadable
 */
class ShopUserSettings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=ShopUsers::class, inversedBy="shopUserSettings", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @Vich\UploadableField(mapping="seller_logo", fileNameProperty="logo")
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $background;

    /**
     * @Vich\UploadableField(mapping="seller_background", fileNameProperty="background")
     * @var File
     */
    private $backgroundFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    /**
     * @Vich\UploadableField(mapping="seller_top_banner", fileNameProperty="banner")
     * @var File
     */
    private $bannerFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $user_picture;

    /**
     * @Vich\UploadableField(mapping="user_picture", fileNameProperty="user_picture")
     * @var File
     */
    private $userPictureFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resultsPicture;

    /**
     * @Vich\UploadableField(mapping="results_picture", fileNameProperty="resultsPicture")
     * @var File
     */
    private $resultsPictureFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?ShopUsers
    {
        return $this->user;
    }

    public function setUser(?ShopUsers $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function setLogoFile(File $image = null)
    {
        $this->logoFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getLogoFile()
    {
        return $this->logoFile;
    }

    public function getBackground(): ?string
    {
        return $this->background;
    }

    public function setBackground(?string $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getResultsPicture(): ?string
    {
        return $this->resultsPicture;
    }

    public function setResultsPicture(?string $resultsPicture): self
    {
        $this->resultsPicture = $resultsPicture;

        return $this;
    }

    public function setBackgroundFile(File $image = null)
    {
        $this->backgroundFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getBackgroundFile()
    {
        return $this->backgroundFile;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function setBannerFile(File $image = null)
    {
        $this->bannerFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getBannerFile()
    {
        return $this->bannerFile;
    }

    public function getUserPicture(): ?string
    {
        return $this->user_picture;
    }

    public function setUserPicture(?string $user_picture): self
    {
        $this->user_picture = $user_picture;

        return $this;
    }

    public function setUserPictureFile(File $image = null)
    {
        $this->userPictureFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getUserPictureFile()
    {
        return $this->userPictureFile;
    }

    public function setResultsPictureFile(File $image = null)
    {
        $this->resultsPictureFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getResultsPictureFile()
    {
        return $this->resultsPictureFile;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function serialize()
    {
        return serialize($this->id);
    }

    public function unserialize($serialized)
    {
        $this->id = unserialize($serialized);
    }
}
