$(document).ready(function() {
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Abbiamo bisogno di conoscere la tua posizione per poterti mostrare i risultati, ricarica la pagina o controlla le impostazioni del tuo smartphone.")
            console.error = "Geolocation is not supported by this browser.";
        }
    }
    function showPosition(position) {

        $.ajax({
            dataType: "json",
            url: 'https://api.bigdatacloud.net/data/reverse-geocode-client',
            data: {
                'latitude': position.coords.latitude,
                'longitude': position.coords.longitude,
                'localityLanguage' : 'it'
            },
            success: function(response) {
                console.success("Client position: " + position.coords.latitude + "," + position.coords.longitude);
                $('#location').text(response.locality + ', ' + response.principalSubdivision + ', ' + response.countryCode);
            }
        });

        localStorage['position_latitude'] = position.coords.latitude;
        localStorage['position_longitude'] = position.coords.longitude;

        $.ajax({
            dataType: "html",
            url: '/api/v3/getSellersList/' + position.coords.latitude + '/' + position.coords.longitude,
            beforeSend: function() {
                $('#results').html('<div class="row"><div class="col-12"><i class="fa fa-spinner fa-spin fa-3x mx-auto" style="position:relative;left: 50%;margin-left: -3em;"></i></div></div>');
            },
            success: function(response, status) {
                $('#results').html(response);
            }
        }).onerror( function () {
            alert('Error');
        });
    }
    getLocation();


})